%% Copyright 2010-2015 Yves de Saint-Pern
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
%
% This work has the LPPL maintenance status `maintained'.
% 
% The Current Maintainer of this work is Yves de Saint-Pern.
%
% This work consists of the following files:
%		droit-fr.tex, droit-fr.cls, droit-fr.bbx, droit-fr.cbx

              +---------------------------------------+
              |                droit-fr               |
              |                                       |
              | LaTeX tools for french thesis in law. |
              |                                       |
              | By Yves de Saint-Pern                 |
              | yves.desaintpern@gmail.com            |
              +---------------------------------------+

Description
===========

droit-fr is a toolkit intented for students writing a thesis in french law.
It features:

- a LaTeX document class solving most issues faced in french law context, such as:
  . numbered paragraphs ("verses")
  . index using verse numbers rather than pages
  . extended number of hierarchical levels
  . two tables of contents with different depths (summary vs. long toc)

- a bibliographic style for Biblatex package:
  . supporting most french legal sources (laws, cases, commentaries)
  . designed from typical french graphical settings

- a practical example of french thesis document based on these two resources.

- a documentation explaining the contents of these resources and how to use them.


Copyright and License
=====================

Copyright 2010-2016 Yves de Saint-Pern

This work may be distributed and/or modified under the conditions of
the LaTeX Project Public License, either version 1.3c of this license
or (at your option) any later version.  The latest version of this
license is in

                http://www.latex-project.org/lppl.txt

and version 1.3c or later is part of all distributions of LaTeX version
2008/05/04 or later.
