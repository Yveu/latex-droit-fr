#!/bin/bash

PACKAGE=droit-fr
WORKDIR=$PWD
SRCDIR=..

#------------------------------------------------------------------------------
# clean sources and destinations
cd $SRCDIR/documentation
latexmk
latexmk -c
rm *~
cd $SRCDIR
latexmk
latexmk -c
rm *~

#------------------------------------------------------------------------------
# create TDS tree and archive
TDSARCHIVE=$PACKAGE.tds.zip
OUTDIR=texmf
STYDIR=$OUTDIR/tex/latex/$PACKAGE
DOCDIR=$OUTDIR/doc/latex/$PACKAGE
EXAMPLEDIR=$DOCDIR/example

cd $WORKDIR
rm -rf $OUTDIR
mkdir -p $STYDIR
mkdir -p $DOCDIR
mkdir -p $EXAMPLEDIR

cp $SRCDIR/$PACKAGE.* $STYDIR
cp $SRCDIR/documentation/* $DOCDIR
cp $SRCDIR/*.tex $SRCDIR/*.bib $SRCDIR/latexmkrc $EXAMPLEDIR

rm $TDSARCHIVE
cd $OUTDIR
zip -r ../$TDSARCHIVE *

#------------------------------------------------------------------------------
# create CTAN package and archive
ARCHIVE=$PACKAGE.zip
OUTDIR=ctan
STYDIR=$OUTDIR/$PACKAGE
DOCDIR=$OUTDIR/$PACKAGE/doc
EXAMPLEDIR=$OUTDIR/$PACKAGE/example

cd $WORKDIR
rm -rf $OUTDIR
mkdir -p $STYDIR
mkdir -p $DOCDIR
mkdir -p $EXAMPLEDIR

cp $SRCDIR/$PACKAGE.* $STYDIR
cp $SRCDIR/documentation/* $DOCDIR
cp $SRCDIR/*.tex $SRCDIR/*.bib $SRCDIR/latexmkrc $EXAMPLEDIR
cp $SRCDIR/$PACKAGE.* $STYDIR
cp $SRCDIR/README $STYDIR
cp $SRCDIR/CHANGELOG $STYDIR
cp $TDSARCHIVE $OUTDIR

rm $ARCHIVE
cd $OUTDIR
zip -r ../$ARCHIVE *



